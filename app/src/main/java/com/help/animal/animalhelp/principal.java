package com.help.animal.animalhelp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class principal extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.toolbar);
    }
    public void Ayuda (View view){
        Intent ayuda= new Intent(this , Ayuda.class);
        startActivity(ayuda);
    }
    public void perdida (View view){
        Intent perdida = new Intent(this ,perdido.class);
        startActivity(perdida);
    }
}
