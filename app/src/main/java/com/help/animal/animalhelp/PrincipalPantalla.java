package com.help.animal.animalhelp;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class PrincipalPantalla extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    ImageView imagen;
    Button boton;
    Button CargarIMg;
    static final int REQUEST_IMAGE_CAPTURE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal_pantalla);

        imagen = (ImageView) findViewById(R.id.imgCargada);
        boton = (Button) findViewById(R.id.btnFoto);
        if (validarPermiso()){
           boton.setEnabled(true);
        }else{
        boton.setEnabled(false);
        }


        boton = (Button) findViewById(R.id.btnFoto);

        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llamarIntent();
            }
        });

    }

    private boolean validarPermiso() {
            if(Build.VERSION.SDK_INT<Build.VERSION_CODES.M){
                return true;
            }
            if ((checkSelfPermission(CAMERA)==PackageManager.PERMISSION_GRANTED&&(checkSelfPermission(WRITE_EXTERNAL_STORAGE)==PackageManager.PERMISSION_GRANTED))){
            return true;
            }
            if ((shouldShowRequestPermissionRationale(CAMERA))||(shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE))){
                cargarDialogoRecomendacion();
        }else{
               requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,CAMERA},100);
            }
        return false;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 100){
            if (grantResults.length==2 && grantResults[0] ==PackageManager.PERMISSION_GRANTED && grantResults[1] ==PackageManager.PERMISSION_GRANTED){
              boton.setEnabled(true);
            }else {
                solicitarPermisoManual();
            }
        }
    }

    private void solicitarPermisoManual() {

    }

    private void cargarDialogoRecomendacion() {
        AlertDialog.Builder dialogo= new AlertDialog.Builder(PrincipalPantalla.this);
        dialogo.setTitle("Permisos Desactivados");
        dialogo.setMessage("Debe aceptar los permisos para el correcto funcionamiento de la App");

        dialogo.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,CAMERA},100);
            }
        });
        dialogo.show();
    }

    private void llamarIntent() {
    Intent tomarFoto = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
    if (tomarFoto.resolveActivity(getPackageManager()) != null) {
        startActivityForResult(tomarFoto, REQUEST_IMAGE_CAPTURE);
    }
}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
           Bundle extras = data.getExtras();
            Bitmap imgBitmap = (Bitmap) extras.get("data");
            imagen.setImageBitmap(imgBitmap);
        }
        }

    }


